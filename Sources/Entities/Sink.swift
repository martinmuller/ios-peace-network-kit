//
//  Sink.swift
//  PeaceNetworkKit
//
//  Created by Martin Muller on 27/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import ObjectMapper

public struct Sink: Mappable {
    
    public init?(map: Map) {  }
    
    public func mapping(map: Map) {
        
    }
    
    public static func empty(_ data: [String: String] = [:]) -> Sink {
        let json = "{}"
        let jsonDictionary = Mapper<Sink>.parseJSONString(JSONString: json)
        let mapData: Map = Map(mappingType: .fromJSON, JSON: jsonDictionary! as! [String : Any], toObject: true)
        return Sink(map: mapData)!
    }
}
