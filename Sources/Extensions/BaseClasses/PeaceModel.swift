//
//  PeaceModel.swift
//  PeaceNetworkKit
//
//  Created by Martin Muller on 26/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import PeaceKit
import Moya
import SwiftyJSON

public typealias PeaceWebErrorHandler = (_ statusCode: Int, _ response: JSON) -> Void

extension PeaceModel {
    open func handleError(_ rawError: Swift.Error, action: PeaceWebErrorHandler) -> String {
        let error = rawError as! Moya.MoyaError // swiftlint:disable:this force_cast
        
        let message: String
        var statusCode: Int = 0
        if let response = error.response {
            statusCode = response.statusCode
            do {
                let rawJson = try response.mapJSON()
                let json = JSON(rawJson)
                
                action(response.statusCode, json)
                
                if let errorMessage = json["error"].string {
                    message = errorMessage
                } else {
                    if let errors = json["error"].dictionary {
                        if errors.count > 0 {
                            if let errorTuple = errors.first {
                                let (field, reasonOpt) = errorTuple
                                if let reason = reasonOpt.string {
                                    message = "\(field.capitalized) \(reason)"
                                } else {
                                    message = "\(field.capitalized) is invalid"
                                }
                            } else {
                                message = "[PeaceKit] Response JSON 'error' key first item is empty"
                            }
                        } else {
                            message = "[PeaceKit] Response JSON 'error' key does not contain any errors"
                        }
                    } else {
                        message = "[PeaceKit] Response JSON does not contain 'error' key"
                    }
                }
            } catch {
                message = "[PeaceKit] Response \(response.statusCode) from server does not contain JSON"
            }
        } else {
            message = "[PeaceKit] Loading error does not contain Response"
        }
        
        print("[PeaceKit] \(String(describing: type(of: self))): Network Error [\(statusCode)] \(message)")
        
        return message
    }
    
    public static func handleError(_ rawError: Swift.Error, action: PeaceWebErrorHandler) -> String {
        let error = rawError as! Moya.MoyaError // swiftlint:disable:this force_cast
        
        let message: String
        var statusCode: Int = 0
        if let response = error.response {
            statusCode = response.statusCode
            do {
                let rawJson = try response.mapJSON()
                let json = JSON(rawJson)
                
                action(response.statusCode, json)
                
                if let errorMessage = json["error"].string {
                    message = errorMessage
                } else {
                    if let errors = json["error"].dictionary {
                        if errors.count > 0 {
                            if let errorTuple = errors.first {
                                let (field, reasonOpt) = errorTuple
                                if let reason = reasonOpt.string {
                                    message = "\(field.capitalized) \(reason)"
                                } else {
                                    message = "\(field.capitalized) is invalid"
                                }
                            } else {
                                message = "[PeaceKit] Response JSON 'error' key first item is empty"
                            }
                        } else {
                            message = "[PeaceKit] Response JSON 'error' key does not contain any errors"
                        }
                    } else {
                        message = "[PeaceKit] Response JSON does not contain 'error' key"
                    }
                }
            } catch {
                message = "[PeaceKit] Response \(response.statusCode) from server does not contain JSON"
            }
        } else {
            message = "[PeaceKit] Loading error does not contain Response"
        }
        
        print("[PeaceKit] \(String(describing: type(of: self))): Network Error [\(statusCode)] \(message)")
        
        return message
    }
    
    open func networkPlugins(debug: Bool = false, debugResponseData: Bool = false) -> [PluginType] {
        var plugins: [PluginType] = []
        
        if debug == true {
            plugins.append(NetworkLoggerPlugin(verbose: debugResponseData, responseDataFormatter: PeaceJSONDebugRDF))
        }
        
        return plugins
    }
}
