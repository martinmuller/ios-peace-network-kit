//
//  File.swift
//  PeaceNetworkKit
//
//  Created by Martin Muller on 27/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import ObjectMapper

extension Mappable {
    static public func sampleData() -> Data {
        return Self.empty().toJSONString()!.UTF8EncodedData as Data
    }
    
    static public func empty(data: [String: String] = [:]) -> Self {
        let json = "{}"
        let jsonDictionary = Mapper<Self>.parseJSONStringIntoDictionary(JSONString: json)
        let mapData: Map = Map(mappingType: .fromJSON, JSON: jsonDictionary!, toObject: true)
        return self.init(map: mapData)!
    }
}
